import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-tetris',
  templateUrl: './tetris.component.html',
  styleUrls: ['./tetris.component.css']
})
export class TetrisComponent implements OnInit {
  @ViewChild('canvas') public canvas: ElementRef;

  
  private cx: CanvasRenderingContext2D; 

  	private dropCounter: number = 0;
	private dropInterval: number = 1000;

	private lastTime: number = 0;

	private colors: string[] = [
		null,
		'red',
		'blue',
		'green',
		'yellow',
		'brown',
		'purple',
		'orange'
	]

	private arena: any = this.createMatrix(12, 20);

	private player: any = {
		pos: {x: 0, y: 0},
		matrix: null,
		score: 0,
	};

  constructor() { }

  ngOnInit() {
 	const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    this.cx = canvas.getContext('2d');

    canvas.width = 240;
    canvas.height = 400;
    this.cx.scale(20,20);

    this.playerReset();
	//this.updateScore();
	this.update();


  }

  	arenaSweep(){
		let rowCount = 1;
		outer: for(let y = this.arena.length - 1; y > 0; --y){
			for(let x = 0; x < this.arena[y].length; ++x){
				if (this.arena[y][x] === 0) {
					continue outer;
				}
			}

			const row = this.arena.splice(y, 1)[0].fill(0);
			this.arena.unshift(row);
			y++;

			this.player.score += rowCount * 10;
			rowCount *= 2;
		}
	}

	collide(arena, player){
		const [m, o] = [player.matrix, player.pos];
		for (let y = 0; y < m.length; ++y) {
			for (let x = 0; x < m[y].length; ++x) {
				if (m[y][x] !== 0 && (arena[y + o.y] && arena[y + o.y][x + o.x]) !== 0) {
					return true;
				}
			}
		}
		return false;
	}

	createMatrix(w,h){
		const matrix = [];
		while (h--) {
			matrix.push(new Array(w).fill(0))
		}
		return matrix;
	}

	createPiece(type){
		switch(type){
			case 'T' : {
				return [
							[0,0,0],
							[1,1,1],
							[0,1,0],
						];
			};
			case 'O' : {
				return [
							[2,2],
							[2,2],
						];
			};
			case 'L' : {
				return [
							[0,3,0],
							[0,3,0],
							[0,3,3],
						];
			};
			case 'J' : {
				return [
							[0,4,0],
							[0,4,0],
							[4,4,0],
						];
			};
			case 'I' : {
				return [
							[0,5,0,0],
							[0,5,0,0],
							[0,5,0,0],
							[0,5,0,0],
						];
			};
			case 'S' : {
				return [
							[0,6,6],
							[6,6,0],
							[0,0,0],
						];
			};
			case 'Z' : {
				return [
							[7,7,0],
							[0,7,7],
							[0,0,0],
						];
			};
		}	
	}

	draw(){
		this.cx.fillStyle = '#000';
		this.cx.fillRect(0,0,this.canvas.nativeElement.width, this.canvas.nativeElement.height);
		this.drawMatrix(this.arena, {x: 0, y: 0});
		this.drawMatrix(this.player.matrix, this.player.pos)
	}

	drawMatrix(matrix, offset){
		matrix.forEach((row, y) => {
			row.forEach((value,x) => {
				if (value !== 0) {
					this.cx.fillStyle = this.colors[value];
					this.cx.fillRect(x + offset.x, y + offset.y, 1, 1);
				}
			});
		});
	}

	merge(arena, player){
		player.matrix.forEach((row, y) => {
			row.forEach((value, x) => {
				if (value !== 0) {
					arena[y + player.pos.y][x + player.pos.x] = value;
				}
			});
		});
	}

	playerDrop(){
		this.player.pos.y++;
		if (this.collide(this.arena, this.player)) {
			this.player.pos.y--;
			this.merge(this.arena, this.player);
			this.playerReset();
			this.arenaSweep();
			//this.updateScore();
		}
		this.dropCounter = 0;
	}

	playerMove(dir){
		this.player.pos.x += dir;
		if (this.collide(this.arena, this.player)) {
			this.player.pos.x -= dir;
		}
	}

	playerReset(){
		const pieces = 'ILJOTSZ';
		this.player.matrix = this.createPiece(pieces[pieces.length * Math.random() | 0]);
		this.player.pos.y = 0;
		this.player.pos.x = (this.arena[0].length / 2 | 0) - (this.player.matrix[0].length / 2 | 0);
		if (this.collide(this.arena, this.player)) {
			this.arena.forEach(row => row.fill(0));
			this.player.score = 0;
			//this.updateScore();
		}
	}

	playerRotate(dir){
		const pos = this.player.pos.x;
		let offset = 1
		this.rotate(this.player.matrix, dir);
		while(this.collide(this.arena, this.player)){
			this.player.pos.x += offset;
			offset = -(offset + (offset > 0 ? 1 : -1));
			if (offset > this.player.matrix[0].length) {
				this.rotate(this.player.matrix, -dir);
				this.player.pos.x = pos;
				return;
			}
		}
	}

	rotate(matrix, dir){
		for(let y = 0; y < matrix.length; ++y){
			for(let x = 0; x < y; ++x){
				[
					matrix[x][y],
					matrix[y][x],
				] = [
					matrix[y][x],
					matrix[x][y],
				];
			}
		}
		if (dir > 0) {
			matrix.forEach(row => row.reverse());
		} else {
			matrix.reverse();
		}
	}

	update(time = 0){
		const deltaTime = time - this.lastTime;
		this.lastTime = time;
		this.dropCounter += deltaTime;
		if (this.dropCounter > this.dropInterval) {
			this.playerDrop();
		}
		this.draw();
		requestAnimationFrame(this.update.bind(this));
	}

	onKeyDown(event){
		switch (event.keyCode) {
			case 37:
				this.playerMove(-1);
				break;
			case 39:
				this.playerMove(1);
				break;
			case 40:
				this.playerDrop();
				break;
			case 81:
				this.playerRotate(-1);
				break;
			case 87:
				this.playerRotate(1);
				break;
			default:
				console.log("that key does nothing")
				break;
		}
	}

	// updateScore(){
	// 	document.getElementById('score').innerText = this.player.score;
	// }

}
